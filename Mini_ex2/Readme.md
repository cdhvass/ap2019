https://cdn.staticaly.com/gl/cdhvass/ap2019/raw/master/Mini_ex2/sketch%20+%20index/index.html





![screenShot](Skærmbillede1.png)

![screenShot](Skærmbillede2.png)



Emoji icons have and has had a big influence on the society in which they are used and vice versa. 
It was interesting to read Modifying the Universal by Roel Roscam Abbing, Peggy Pierrot, Femke Snelting 
because it gave an insight to have much the society actually effects the emojis chosen to be part of the alternative keyboard. 
I thought it was an important topic the text dealt with, 
but I was not aware that it had been such a difficult task for the unicode consortium. 
I chose to make this p5.js program as a comment on the racism that are unfortunately still a part of the 21st century.
While reading the text I discovered that it might also be seen as a support to the Black Lives Matter organisation, which they mention. 
I think it is important that everyone using the emojis feel represented 
and I now see the evolvement of the emojis as a reflection of were the society is currently at. 
I just read that women are now getting a menstruation emoji, which I think is nice, but I was sort of disappointed by the fact that it will be a drop of blood. This show that society is not quite ready to talk about menstruation and I think this a good example of how the society effects the emoji. 

I chose to start the program of with a black background to get the feeling of being in a dark room. The text I chose to be dark grey so that it would not disrupt the illusion of the darkness. While writing this readme I have come to think about the fact that "the dark" could also implicate evil, which was not my intention with te program. I have thought of the mousePressed function a sort of turning on the light. When the mouse is pressed you will be able to see all of the emojis and if you press the mouse again the “light” will turn of and the emojis will turn black. This I hope creates the illusion of the emojis still being in the “room”. A cool feature I would have liked to add was the emojis changing color every time the “light” turned on. But I could not quite figure out how to do so.  

To make this program a used the p5.js reference page a lot. After making the background and the circles I wanted to figure out how to change the color of the circle by some sort of function. I chose mousePressed because it already had the wanted function in the references. After some struggling I learned that I had to create a variable for each emoji, to make them change color individually. For the eyes I drew some smaller circles and for the mouth I originally tried to use the arc() function, but it did not work out as I wanted it to. Therefore I used curveVertex and changed the points until I was satisfied. In this mini_ex I also chose to use the //comment function, which helped me organize the code. While watching the assigned videos for class 03, specifically 5.2, I learned that I could have made my code shorter and simpler by using parameters and arguments. These I am excited to get to use and experiment with in future projects.  



