//emoji
let value1 = 0;
let value2 = 0;
let value3 = 0;
let value4 = 0;
let value5 = 0;

//text
let value6 = 30;


function setup() {
  createCanvas(windowWidth, windowHeight);
  }

function draw() {
  background(0)

//emoji1
  fill(value1);
  circle(200,200,85);
//eyes
  fill(0);
  circle(230, 180, 6); //right
  circle(170, 180, 6); //left
//mouth
  noFill();
  beginShape();
    curveVertex(100,55);
    curveVertex(150,230);
    curveVertex(250,230);
    curveVertex(400,55);
  endShape();


//emoji2
  fill(value2);
  circle(400,400,50);
//eyes
  fill(0);
  circle(420, 390, 5); //right
  circle(390, 390, 5); //left
//mouth
  noFill();
  beginShape();
    curveVertex(350,300);
    curveVertex(430,415);
    curveVertex(375,415);
    curveVertex(400,150);
    endShape();


//emoji3
  fill(value3);
  circle(800,250,100);
//eyes
  fill(0);
  circle(830, 225, 6); //right
  circle(770, 225, 6); //left
//mouth
  noFill();
  beginShape();
    curveVertex(700,20);
    curveVertex(740,275);
    curveVertex(870,275);
    curveVertex(800,20);
  endShape();


//emoji4
  fill(value4);
  circle(1300,150,80);
//eyes
  fill(0);
  circle(1330, 140, 6); //right
  circle(1280, 140, 6); //left
//mouth
  noFill();
  beginShape();
    curveVertex(1300,10);
    curveVertex(1350,175);
    curveVertex(1250,175);
    curveVertex(1200,10);
  endShape();


//emoji5
  fill(value5);
  circle(1200,500,30);
//eyes
  fill(0);
  circle(1210, 495, 4); //right
  circle(1185, 495, 4); //left
//mouth
  noFill();
  beginShape();
    curveVertex(1180,450);
    curveVertex(1180,510);
    curveVertex(1215,510);
    curveVertex(1200,450);
  endShape();

//text
  fill(value6);
  textSize(40);
  text('In the dark we all look alike', 200, 50);

  }


function mousePressed() {

//emoji1
if (value1 === 0) {
    value1="#61390f";
  } else {
    value1 = 0;
  }


//emoji2
if (value2 === 0) {
    value2 = "#fdd091";
  } else {
    value2 = 0;
  }

//emoji3
if (value3 === 0) {
    value3 = "#99613d";
  } else {
    value3 = 0;
  }


//emoji4
if (value4 === 0) {
      value4 = "#feb287";
    } else {
      value4 = 0;
    }

//emoji5
if (value5 === 0) {
    value5 = "#291500";
  } else {
    value5 = 0;
  }

//text
if (value6 === 30) {
    value6 = 255;
  } else {
    value6 = 30;
  }

  }
