**MINI_EX 10**

For this mini_ex I used my mini_ex3 for the flowchart. This is the link to the program:
https://cdn.staticaly.com/gl/cdhvass/ap2019/raw/master/Mini_ex3/sketch%20+%20index/index.html

and a link to the folder:
https://gitlab.com/cdhvass/ap2019/tree/master/Mini_ex3

![screenShot](Skærmbillede1.png)

**What may be the difficulty in drawing the flow chart?**

While making this flowchart I was thinking very similar to when I use the comments in p5.js. I have used the comments to make it easier for me and the reader to understand what the specific syntaxes and lines of code do, which I guess is pretty much the same as with the flowchart: a simplification of the program. The difficulty of drawing this flowchart might be that none of my programs has an actual ending because most of them are just looping indefinitely. Therefore it will not be quite clear where to end the program and I am not actually sure wether or not a flowchart can just be a loop, since my programs are. 

**GROUP FLOWCHARTS**

Throbber:

![screenShot](Skærmbillede2.png)

Game:
![screenShot](Skærmbillede3.png)

**Group: What might be the possible technical challenges for the two ideas and how are you going to solve them?**
By far the most challenging program of the two would be our game idea “Hackman” (working title). As for the throbber idea, it is fairly simple and we already have a good idea on how the program would work. 

Among the challenges that “Hackman” presents, one which we consider the most potentially troublesome would be getting every part of the program to interact as intended and cooperate with one another. This is mainly due to the overall workload that would go into the creation of the program, which is more than we have dealt with previously. In correlation, we suspect that the amount of barriers, walls, objects and sprites might pose a challenge. The solution should however not be out of reach with the help of the play.library and a workforce of all three group members. 
Other challenges could be gameplay-wise, such as adding changes between levels, a progressive system, and the ability to buy upgrades. 



**Individual: How is this flow chart different from the one that you had in #3? If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms?** 
These flowcharts were different from my individual one, since we used them to help us generate a specific idea, where as I made my flowchart more as a analyzation of my already existing program. Another difference is that the flowchart for the game actually has an end (because you either win or lose), but because the other flowchart is for throbber (with no real loading function) it continues to run, which is similar to my individual flowchart.  
In computer science algorithms are usually some sort of guidance or “rule set” for a computer to follow to execute a specific task and in our group flowcharts worked quit similar. We used them as a way to make sure that we were all agreeing on which way the program should run. It was a easier way to grasp the task of brainstorming and specifying different ideas, than just talking about them. We used the flowcharts as map of what the programs would do, which made it easy to stop and focus on the different stages of the program, to prevent errors or simply to improve, before even producing it.
