![screenShot](Skærmbillede.png)




https://cdn.staticaly.com/gl/cdhvass/ap2019/raw/master/Mini_ex1/sketch%20+%20index/index.html



***  Describe your first independent coding process**
Since most of my experience with coding leading up to this mini-ex has been with still and non-moving objects I now wanted to make something move. 
Initially I used the p5.js code for a 3D box, which is not only the box, but also the rotation. 
I copied and inserted the code five times and then I changed four of the boxes to be spheres and one to be a torus 
(all of these codes were also found in the p5.js library). 
To alter the positions of the five objects I used the code translate(100,100,100). 
I was not sure what the different numbers meant, so I changed them each individually to see the effect of each number. 
Afterwards I gave the objects the positions as so they were in line. 
For the coloring I used let c=color() for the torus and then I discovered that there was a code where I could keep the same basecolor for all of the object, 
but alter the brightness of them individually. 
I did so, so that the torus is the brightest and each sphere is darker than the one before. 
Though I made an error in the last sphere, where I typed 600% instead of 60%. 
The color of the background I chose because I found it to be a good contrast to the objects.



***  How your coding process is differ or similar to reading and writing text?**
My coding process was different from a writing process because I copied most of my coding and changed it a bit. 
Other than that the process was also different because the coding has to be very specific. 
This is sort of similar to grammatical errors in a writing process, 
but the difference is that a grammatical error does not effect the whole product the way a coding error does. 

***  What is code and coding/programming practice means to you?**
For me coding is a fairly new method of creating. 
I have have tried coding and programming before, but always with a specific task in mind. 
So I really enjoyed just being able to explore and play around with different types of codes and not worry so much about the result, 
but rather see this mini-ex as an learning experience. 