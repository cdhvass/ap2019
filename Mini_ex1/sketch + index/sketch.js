


function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL );
}

function draw() {
  background(200,100,100);

  rotateX(frameCount * 0.03);
   rotateY(frameCount * 0.03);
   translate (100,100,100)
   let c = color(0, 200, 200);
   c = color('hsl(160, 100%, 50%)');
   fill(c);
torus(50, 15);

  rotateX(frameCount * 0.01);
  rotateZ(frameCount * 0.01);
translate (100,100,110)
c = color('hsl(160, 90%, 50%)');
fill(c);
  sphere(30);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
translate (100,100,120)
c = color('hsl(160, 80%, 50%)');
fill(c);
  sphere(40);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
  translate (100,100,130)
  c = color('hsl(160, 70%, 50%)');
  fill(c);
  sphere(50);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
  translate (100,100,140)
  c = color('hsl(160, 60%, 50%)');
  fill(c);
  sphere(60);
}
