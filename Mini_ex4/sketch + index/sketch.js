  let img; //male
  let bil; //female
  var man;
  var woman;
  var boy;
  var girl;

function preload() {
    img = loadImage('man.jpg');
    bil = loadImage ('woman.jpg');
  }

function setup() {
  createCanvas(windowWidth, windowHeight);
    textSize(50);
    text('Who are you?', 50,50);
    fill(0);
}

function draw() {
    man = createButton ('MAN');
    man.position(100,100);
    man.mousePressed (MAN);

    woman = createButton ('WOMAN');
    woman.position(250,100);
    woman.mousePressed(WOMAN)

    boy = createButton ('BOY');
    boy.position(100,200);
    boy.mousePressed (BOY);

    girl = createButton ('GIRL');
    girl.position(250,200);
    girl.mousePressed (GIRL);
}

 function MAN() {
  createCanvas(windowWidth,windowHeight);
    background(0,100,200);
    image(img, width/2, height/2,150,300);
    textSize(50);
    fill(0,255, 0);
    text('This is you!', width/2,50);
 }

function WOMAN(){
  createCanvas(windowWidth,windowHeight);
    background(200,0,100);
    image (bil,width/2,height/2,150,300);
    textSize(50);
    fill(0,255,0);
    text('This is you!', width/2,50);
}

function GIRL(){
  createCanvas(windowWidth,windowHeight);
    background(200,0,100,200);
    image (bil,width/2,height/2,100,200);
    textSize(50);
    fill(0,255,0);
    text('This is you!', width/2,50);
}

function BOY(){
  createCanvas(windowWidth,windowHeight);
    background(0,200,200,200);
    image (img,width/2,height/2,100,200);
    textSize(50);
    fill(0,255,0);
    text('This is you!', width/2,50);
}
