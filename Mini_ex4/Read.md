
https://cdn.staticaly.com/gl/cdhvass/ap2019/raw/master/Mini_ex4/sketch%20+%20index/index.html
![screenShot](Skærmbillede1.png)
![screenShot](Skærmbillede2.png)



My thought behind this mini_ex was to show the absurdity of the data we a asked to give online. 
I wanted to make some sort of quiz where the computer guesses who the users are by using if and then syntaxes, 
but I could not make it work the way I wanted it to. So the program is not at all to my satisfactory. 
I also wanted to use the radio buttons for this mini_ex, but it did not work for me, when I altered the p5.js DOM library syntaxes. 
My result of the different choices of gender, was the same as I originally wanted it to be. 
Since the only questions we are ever asked or able to answer, when for example making a profil somewhere are limited and shallow, 
the figure at the end will be the same. Either you are a man, a woman, a boy or a girl. 
According to many websites, when creating a profil, there is no in-between. Since this weeks assignment was "Capture All", 
this mini_ex just shows how software or computers in general, rarely are able to actually "capture all".
That is also why I ended up with this result, because I like the simplicity of it. 