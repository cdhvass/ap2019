let x = 0;
let y = 0;
let spacing = 10;


function setup() {
  createCanvas(windowWidth,windowHeight);
  background(0);
}

function draw() {
  noStroke();

  if (random(1) < 0.7) {

//the triangles (red/blue lines)
    fill(random(255),0,random(255));
    triangle(x,y,width,height,width+spacing,height+spacing);

  } else {

//the rectangles
    fill(20,random(255),20);
    rect(x+spacing,y+spacing,10,30);
  }

//make a new row
    x+=11
  if (x > width) {
    x = 0;
    y += spacing;
  }

//go back to the top
  if (y >= height) {
    x = 0;
    y = 0;
  }

}
