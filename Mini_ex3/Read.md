https://cdn.staticaly.com/gl/cdhvass/ap2019/raw/master/Mini_ex3/sketch%20+%20index/index.html





![screenShot](Skærmbillede1.png)
![screenShot](Skærmbillede2.png)



For this mini_ex I based my coding on Winnies example of a throbber. I copied her example, but deleted everything except the throbber and the backgrounds. I then alteret the throbber by playing with the color, shape and number of objects. I chose the syntax quad() because I liked the snake-like look of it. I used an array to make the different sentences and defined index as a variable to be able to use it in all of the sentences. I wanted the sentences to chance each time the mouse was moved to a different part of the canvas. I split the canvas in four (vertical and horizontal) by making individual constrains for the different areas. To get the sentences to chance randomly I had to make two variables for each of the areas. One of them to make sure the area started up being blank. I used push() and pop() to make sure the different syntaxes would not effect each other, since I wanted all of the sentences to be different. I also made it so that the area of which the mouse was in is not the same as the sentence to make it a little more interesting to “wait”. I tried to base the sentences in this program on the assigned readings for class 03. For example “Maybe it’s loading, maybe it’s not” is related to the part of the text that concerns a false delay of the loading process. I was not aware of this fact before reading the text and thought it would be a neat thing to include in this program, to make the user consider the fact that the throbber might not always just show that something is loading. 

I have always been kind of annoyed by throbbers and loading bars, especially when I was watching videos. Before reading the text for this week I had always considered throbbers and loading bars as something telling me that a process was going on. I was not aware of what was really going on nor could I grasp what actually needed to happen for me to continue my process. I had never considered the fact that nothing might be going on. Since reading the text I have started to consider the meaning behind the throbber more than before. 