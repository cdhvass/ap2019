var words = ["Close your eyes, maybe it'll go faster.", "Jump 3 times and it might be loaded.", "Keep calm and keep loading.",
 "Take a nap, this will take a while.", "Maybe it's loading, maybe it's not."];

var index = 0;

//upLeft
var upL =0;
var wordupL = "";

//upRight
var upR = 0;
var wordupR ="";

//lowLeft
var lowL = 0;
var wordlowL ="";

//lowRight
var lowR = 0;
var wordlowR = "";


function setup() {
 createCanvas(windowWidth, windowHeight);
 background(10);
 frameRate (11); //the speed of the rotation
}

function draw() {

  fill(10,80);
  noStroke();
  rect(0, 0, width, height);
  drawThrobber(11); //number of quads


//upLeft
push();
if (mouseX > 0 && mouseX < width/2 && mouseY > 0 && mouseY < height/2) {
  wordupL++;
  wordupR = 0;
  wordlowL = 0;
  wordlowR = 0;
  fill (100,0,200);
  textSize(40);

  if(wordupL == 1) {
    word = random(words);
  }
  text(word,width/2+50,200); //shown in upRight
}
pop();

//upRight
push();
if (mouseX > width/2 && mouseX < width && mouseY > 0 && mouseY < height/2) {
wordupR++;
wordupL = 0;
wordlowL = 0;
wordlowR = 0;
  fill (200,0,200);
  textSize(40);

  if(wordupR == 1) {
    word = random(words);
  }
  text(word,50,height-200); //shown in lowLeft
}
pop();

//lowLeft
push();
if (mouseX > 0 && mouseX < width/2 && mouseY > height/2 && mouseY < height) {
  wordlowL++;
  wordupL = 0;
  wordupR = 0;
  wordlowR = 0;

  fill (255,0,100);
  textSize(40);

  if(wordlowL == 1) {
    word = random(words);
  }

  text(word,width/2+50, height-200); //shown in lowRight
}
pop();

//lowRight
push();
if (mouseX >  width/2 && mouseX < width && mouseY > height/2 && mouseY < height) {
  wordlowR++;
  wordupL = 0;
  wordlowL = 0;
  wordlupR = 0;
  fill (200,0,200);
  textSize(40);

  if(wordlowR == 1) {
    word = random(words);
  }

  text(word,50,200); //shown in upLeft
}
pop();

}

//the throbber
function drawThrobber(num) {
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255,0,200);
  quad(38, 31, 86, 20, 69, 63, 30, 76);
  pop();

}
