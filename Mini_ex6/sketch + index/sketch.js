let sky = []; // array of stars
let snowflakes = []; //SNOWFLAKES

//TONGUE
let x = 1;
let y = 1;
let easing = 0.3; //how fast the tongue moves

function setup() {
  noStroke();
  createCanvas(windowWidth, windowHeight);

//create stars
  for (let s = 0; s < 100; s++) {
    sky.push(new stjerne());
  }

}

function draw() {
  background(10,10,50);

//STARS
  for (let s = 0; s < sky.length; s++) {
    sky[s].move();
    sky[s].display();
  }

//MOUNTAINS
  push()
    fill(30)
    beginShape();
    vertex(0,height);
    vertex(200,height/2);
    vertex(355,height);
    endShape();

    beginShape();
    vertex(300,height);
    vertex(400,height-100);
    vertex(550,height);
    endShape();

    beginShape();
    vertex(450,height);
    vertex(570,height-170);
    vertex(650,height);
    endShape();

    beginShape();
    vertex(600,height);
    vertex(width/2,height-50);
    vertex(width/2+50,height);
    endShape();

    beginShape();
    vertex(width/2,height);
    vertex(width/2+200,height-400);
    vertex(width/2+400,height);
    endShape();

    beginShape();
    vertex(width/2+300,height);
    vertex(width/2+400,height-150);
    vertex(width/2+500,height);
    endShape();

    beginShape();
    vertex(width/2+475,height);
    vertex(width/2+550,height-200);
    vertex(width/2+625,height);
    endShape();

    beginShape();
    vertex(width/2+600,height);
    vertex(width/2+675,height-75);
    vertex(width+20,height);
    endShape();
  pop();

//TONGUE
  push()
    fill(225,112,147);
    let targetX = mouseX;
    let dx = targetX - x;
    x += dx * easing;

    let targetY = mouseY;
    let dy = targetY - y;
    y += dy * easing;

    ellipse(x, height, 100, 150);
  pop();

//SNOWFLAKES
  let t = frameCount / 400;

// create a random number of snowflakes each frame
  for (let i = 20; i < random(21); i++) {
    snowflakes.push(new snowflake()); // append snowflake object
  }

// loop through snowflakes with a for..of loop
  for (let flake of snowflakes) {
    flake.update(t); // update snowflake position
    flake.display(); // draw snowflake
  }

}
