let emoji = []; // array of emoji objects

function setup() {
  createCanvas(windowWidth, windowHeight);

  // Create objects
  for (let i = 0; i < 10; i++) {
    emoji.push(new Emoji());
  }
}

function draw() {
  background(100);

  for (let i = 0; i < emoji.length; i++) {
    emoji[i].move();
    emoji[i].display();
  }
}

// Emoji class
class Emoji {
  constructor() {
    this.x = random(width);
    this.y = random(height);
    this.diameter = random(50, 100);
    this.speed = 1;
  }

  move() {
    this.x += random(-this.speed, this.speed);
    this.y += random(-this.speed, this.speed);
  }

  display() {
    face (this.x, this.y, this.diameter, this.diameter);
  }
}


function face (x,y,r){
  //emoji1
    fill(220,200,200);
    circle(x,y,r);
  //eyes
    fill(0);
    circle(230, 180, 6); //right
    circle(170, 180, 6); //left
  //mouth
    noFill();
    beginShape();
      curveVertex(100,55);
      curveVertex(150,230);
      curveVertex(250,230);
      curveVertex(400,55);
    endShape();
}
