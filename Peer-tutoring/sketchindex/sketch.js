let img;
var vid;
var playing = false;

function preload() {
  img = loadImage('image/aarhus.jpg');
}
function setup() {
createCanvas(windowWidth,windowHeight);

vid = createVideo(['video1.mp4', 'video2.mov']);
vid.hide();

}

function draw() {

if (mouseIsPressed) {
background(255);
imageMode (CENTER);
image(vid,width/2,height/2);

textSize(50);
text('VIDEO', (width/2+50), 50);
fill(255, 30, 70);
} else {

  imageMode(CORNER);
   tint(0, 153, 204, 126);
  image(img,width/2,0,1000,1044, 1000, 0, width, 1044);

  noTint();
  image(img,0,0,width/2,1044,0,0,1000,1044);

  textSize(50);
  text('IMAGE', (width/2-200), 50);
  fill(255, 30, 70);

}
}

function mousePressed() {
  if (!playing) {
    vid.loop();
        playing = true;
      }
    }

function mouseReleased() {
  if (playing) {
    vid.pause();
    playing = false;
  } }
